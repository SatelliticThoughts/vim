from shutil import copyfile
from pathlib import Path
import os
import requests
import git


SRC="vimrc"
DST=str(Path.home()) + "/.vimrc"

VIM=str(Path.home()) + "/.vim"
AUTOLOAD=VIM + "/autoload"
BUNDLE=VIM + "/bundle"

URL_PATHOGEN="https://tpo.pe/pathogen.vim"
PATHOGEN=AUTOLOAD + "/pathogen.vim"

URL_AIRLINE=""


def create_dir(d):
	print("Creating directory", d)
	os.makedirs(d, exist_ok=True)


def install_pathogen(url, plugin):
	print("Downloading", url)
	r = requests.get(url)
	print("Installing", plugin)
	f = open(plugin, "w")
	f.write(r.content.decode("utf-8"))
	f.close()


def install_plugin(url):
	print("Cloning", url)
	git.Git(BUNDLE).clone(url)


if __name__ == "__main__":
	print("Copying vimrc to ~/.vimrc")
	copyfile(SRC, DST)

	create_dir(VIM)
	create_dir(AUTOLOAD)
	create_dir(BUNDLE)

	install_pathogen(URL_PATHOGEN, PATHOGEN)

	install_plugin("https://github.com/vim-airline/vim-airline")
