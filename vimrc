execute pathogen#infect()

syntax enable
filetype indent on

set relativenumber
set numberwidth=6
set tabstop=4
set cursorline
set colorcolumn=81
highlight ColorColumn ctermbg=Blue guibg=Blue

nnoremap j gj
nnoremap k gk
inoremap jk <esc>

