# Vim Configuration

My vim set up.

## Set Up

To initialise and update vim config

```
python3 setup.py
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
